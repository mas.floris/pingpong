""" This module define a kafka consumer class based on confluent_kafka Consumer"""
from configparser import ConfigParser
from config import Config
from confluent_kafka import Consumer, KafkaException

from helpers import logger, utils

log = logger.get_logger_by_env(Config.APP_ENV)
log = logger.get_logger_by_env(Config.APP_ENV)

# consumer configuration
config_parser = ConfigParser(interpolation=None)
config_file = open("config.properties", "r", encoding="utf8")
config_parser.read_file(config_file)

consumer_config = {"bootstrap.servers": Config.BROKER_ADDRESS}
consumer_config.update(config_parser["consumer"])


class MessageConsumer(Consumer):
    """
    Generic kafka consumer
    """

    def __init__(self) -> None:
        super().__init__(consumer_config)

    consumer = Consumer(consumer_config)

    def read(self, handler):
        """
        Read message from subscribed topics
        """
        msg = self.poll(timeout=1.0)
        if msg is None:
            return
        if msg.error():
            raise KafkaException(msg.error())
        else:
            # Proper message
            # self._handle_input_topic_message(msg.value())
            log.info(
                "Message received by %s %s @ %s",
                msg.topic(),
                msg.partition(),
                msg.offset(),
            )
            log.debug("message: %s", utils.binary_to_utf8(msg.value()))
            handler(msg.value())

            # Store the offset associated with msg to a local cache.
            self.store_offsets(msg)
