"""This module define a custom logger"""

import logging

COMPLEX_FORMAT = (
    "[%(asctime)s] %(levelname)s - %(filename)s - %(funcName)s.%(lineno)d - %("
    "message)s"
)
DATEFORMAT = r"%m-%dT%H:%M:%S"

complex_formatter = logging.Formatter(COMPLEX_FORMAT, DATEFORMAT)
logging.basicConfig(format=COMPLEX_FORMAT, datefmt=DATEFORMAT)


def get_logger(name, log_file=None, level=logging.DEBUG):
    """get custom logger"""
    logger = logging.getLogger(name)
    logger.setLevel(level)
    if log_file:
        file_handler = logging.FileHandler(log_file)
        file_handler.setFormatter(complex_formatter)
        logger.addHandler(file_handler)
    return logger


def get_general_logger():
    """get INFO level logger"""
    logger = logging.getLogger("general_logger")
    logger.setLevel(logging.INFO)
    return logger


def get_debug_logger():
    """get DEBUG level logger"""

    logger = logging.getLogger("debug_logger")
    logger.setLevel(logging.DEBUG)
    return logger


def get_logger_by_env(env):
    """get logger based on APP_ENV var"""
    if env == "dev":
        return get_debug_logger()
    else:
        return get_general_logger()
