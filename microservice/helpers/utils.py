""" This module contains conversion and validation utils """

import json
import jsonschema
from jsonschema import exceptions

def binary_to_utf8(res_bytes):
    """converts binary to utf8"""
    return res_bytes.decode("utf-8")

def binary_to_dict(res_bytes):
    """converts binary str to dict"""
    return json.loads(binary_to_utf8(res_bytes))


def dict_to_json(dictionary):
    """converst dict to json"""
    return json.dumps(dictionary, ensure_ascii=False)


def validate_dict_against_schema(dictionary, schema):
    """validate dict against a json schema"""
    try:
        jsonschema.validate(instance=dictionary, schema=schema)
    except exceptions.ValidationError:
        return False
    return True
