""" This module define the business logic for handling input messages """
from message_producer import MessageProducer
import helpers.utils as utils
from config import Config

from models.event import Event


FORMAT_ERROR_MESSAGE = "Error in the format of the Kafka event"
WRONG_INPUT_ERROR_MESSAGE = (
    f"Error the message doesn’t contain {Config.EXPECTED_MESSAGE}"
)

producer = MessageProducer()


def _on_format_error():
    dict_event = {"message": FORMAT_ERROR_MESSAGE}
    producer.send_event(Config.FAIL_TOPIC, dict_event)


def _on_wrong_message(dict_event):
    dict_event["payload"]["message"] = WRONG_INPUT_ERROR_MESSAGE
    producer.send_event(Config.FAIL_TOPIC, dict_event)


def _on_success(dict_event):
    dict_event["payload"]["message"] = "pong"
    producer.send_event(Config.SUCCESS_TOPIC, dict_event)


def message_handler(msg):
    """send message depending on input"""
    dict_msg = utils.binary_to_dict(msg)

    if not utils.validate_dict_against_schema(dict_msg, Event.schema()):
        _on_format_error()
    elif dict_msg["payload"]["message"] != Config.EXPECTED_MESSAGE:
        _on_wrong_message(dict_msg)
    else:
        _on_success(dict_msg)
