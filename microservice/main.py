""" this module define a api documetation webapp"""
import json

from fastapi import FastAPI, status
from fastapi.exceptions import RequestValidationError
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from models.event import Event

from config import Config

# import producer
from message_producer import MessageProducer


DESCRIPTION = """
## 🚀🚀 API documentation of pingpong microservice endpoints 🚀🚀 ##

"""
app = FastAPI(title="🏓 Pingpong API 🏓", description=DESCRIPTION)
producer = MessageProducer()


@app.post("/event", response_model=Event)
async def send_event(event: Event):
    """post request event to a topic"""
    # maybe add try catch here
    dict_event = event.dict(by_alias=True)
    producer.send_event(Config.INPUT_TOPIC, dict_event)

    return event


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    """ " overrides validatoin exception"""
    # send message to kafka
    producer.send_event(Config.INPUT_TOPIC, json.dumps(exc.body))
    # return validaton error
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=jsonable_encoder({"detail": exc.errors()}),
    )
