""" This module define a kafka producer class based on confluent_kafka Producer"""
from confluent_kafka import Producer
from config import Config

from helpers import logger, utils

log = logger.get_logger_by_env(Config.APP_ENV)
config = {"bootstrap.servers": Config.BROKER_ADDRESS}


class MessageProducer(Producer):
    """
    Generic kafka producer
    """

    def __init__(self) -> None:
        super().__init__(config)

    def delivery_callback(self, err, msg):
        """run commans after producer message delivery"""
        if err is not None:
            log.error("Message failed delivery \n %s", err)
        else:
            log.info(
                "Message delivered to %s %s @ %s",
                msg.topic(),
                msg.partition(),
                msg.offset(),
            )

    def send_event(self, topic, event):
        """sends a kafka event to the specified topic"""
        json_message = utils.dict_to_json(event)
        self.produce(topic, json_message, callback=self.delivery_callback)
        log.info(
                "Message sent to %s",
                topic
            )
        log.debug("message: %s", json_message)
        # Wait up to 1 second for events. Callbacks will be invoked during
        # this method call if the message is acknowledged.
        self.poll(1)
