""" This module define a service to listen to kafka events """

import sys
from config import Config
from message_consumer import MessageConsumer
from message_handler import message_handler

consumer = MessageConsumer()


def start_loop():
    """Start a loop to listen subscribed topics"""
    topics = [Config.INPUT_TOPIC]
    consumer.subscribe(topics)
    try:
        while True:
            consumer.read(message_handler)

    except KeyboardInterrupt:
        sys.stderr.write("%% Aborted by user\n")

    finally:
        # Close down consumer to commit final offsets.
        consumer.close()


if __name__ == "__main__":
    start_loop()
