from pydantic import BaseModel, Field


class EventPayload(BaseModel):
    message: str


class Event(BaseModel):
    transaction_id: str = Field(alias="transaction-id")
    payload: EventPayload
