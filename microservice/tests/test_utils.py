import unittest
from helpers import utils

JSON_SCHEMA = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "transaction-id": {
      "type": "string"
    },
    "payload": {
      "type": "object",
      "properties": {
        "message": {
          "type": "string"
        }
      },
      "required": [
        "message"
      ]
    }
  },
  "required": [
    "transaction-id",
    "payload"
  ]
}

class TestValidateDictAgainstSchema(unittest.TestCase):

    def test_validate_dict_against_schema_success(self):
        dictionary = {"transaction-id": "ID01", "payload": {"message": "ping"}}
        actual = utils.validate_dict_against_schema(dictionary, JSON_SCHEMA)
        expected = True
        self.assertEqual(actual, expected)

    def test_validate_dict_against_schema_fail(self):
        dictionary = {"transaction-id": "ID01", "payloa": {"message": "ping"}}
        actual = utils.validate_dict_against_schema(dictionary, JSON_SCHEMA)
        expected = False
        self.assertEqual(actual, expected)