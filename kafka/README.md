## commands

```sh
# exec bash in container
docker exec -it kafka /bin/bash


# crate topic
kafka-topics.sh --create --topic dev.pingpong.requested --bootstrap-server localhost:9092

# describe topic
kafka-topics.sh --describe --topic dev.pingpong.requested --bootstrap-server localhost:9092

# consume events from topic
kafka-console-consumer.sh --topic dev.pingpong.requested --from-beginning --bootstrap-server localhost:9092

```
