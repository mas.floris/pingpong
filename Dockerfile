FROM python:3.10.9-slim as consumer

WORKDIR /microservice

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY ./microservice /microservice