.DEFAULT_GOAL := help
APP_NAME := pingpong
SHELL :=/bin/bash
HELM_CHART_PATH := kubernetes/helm/app
COMMIT ?= $(shell cut -c-8 <<< `git rev-parse HEAD`)
BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)
POD_NAME := $(shell kubectl get pods --namespace default -l "app.kubernetes.io/name=api,app.kubernetes.io/instance=${APP_NAME}" -o jsonpath="{.items[0].metadata.name}" 2>/dev/null)
CONTAINER_PORT := $(shell kubectl get pod --namespace default ${POD_NAME} -o jsonpath="{.spec.containers[0].ports[0].containerPort}" 2>/dev/null)



.PHONY: help
help:
	@echo "🏓 Welcome to $(APP_NAME)! 🏓"
	@echo ""
	@echo "Use 'make <target>' where <target> is one of:"
	@echo ""
	@echo "  build						build image to in-cluster container runtime"
	@echo "  deploy						build -> deploy-kafka -> deploy-microservice"
	@echo "  delete						uninstall all charts"
	@echo "  expose						expose api at http://127.0.0.1:8080/docs"
	@echo ""
	@echo "🤘 Helm rocks 🤘"


.PHONY: build
build:
	minikube image build -t ${APP_NAME}:${BRANCH}-${COMMIT} .

.PHONY: deploy 
deploy: build
	helm dependency build ${HELM_CHART_PATH} 
	helm install ${APP_NAME} ${HELM_CHART_PATH} --render-subchart-notes --set api.image.tag=${BRANCH}-${COMMIT} --set consumer.image.tag=${BRANCH}-${COMMIT}

.PHONY: delete
delete:
	helm uninstall ${APP_NAME}

.PHONY: expose
expose:
	kubectl --namespace default port-forward ${POD_NAME} 8080:${CONTAINER_PORT}