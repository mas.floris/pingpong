# Ping-Pong Microservice

- [Info](#info)
- [Install](#install)
  - [Dependencies](#dependencies)
  - [Deploy](#deploy)
  - [Configuration](#configuration)
  - [Makefile](#makefile)
  - [Development](#development-docker-compose)
- [Architecture](#architecture)
  - [CI/CD](#ci-cd)
  - [Infrastructure](#infrastructure)
    - [Monitoring](#Monitoring)
      - [Logs](#logs-elk-lokigrafana)
      - [Metrics](#metrics-prometheus-cadvisor)
      - [Exceptions](#exceptions-tracking)
      - [Alerts](#alerts-prometheus-alert-manager-robusta-grafana )
    - [Scale](#scale)
      - [Cluster autoscalig](#cluster-autoscaling)
      - [HPA](#hpa)
### Info

- Pyhton microservice based on the following guidlines:
  - The microservice shall listen to any Kafka event from the following topic: **dev.pingpong.requested**
  - The microservice shall be capable of sending a Kafka event to a topic.
  - The microservice shall be able to choose in which topic to send a Kafka event based on the
    payload of the incoming Kafka event
  - The format of the kafka event must contain two fields: “transaction-id” and “payload”.
  - The microservice shall be portable.
  - The microservice should be containerized and be deployable in a cloud provider

### Install

##### Dependencies

- minikube (v 1.28.0)
- Kubernetes (v 1.25.3)
- Docker (v 20.10.20)
- kubectl (v 1.26.0)
- Helm (v 3.10.3)
- docker-compose v1.29.2 (only for development)


##### Deploy

- Build and deploy the project on a cumernetes cluster


```sh
make deploy
```

- Expose API Documentation to host (local deployment):  
```sh 
make expose
```

Click here to access [API Documentation](http://127.0.0.1:8080/docs)


##### Configuration 
| var  | default value | description  |
|---|---|---|
| APP_ENV  | dev | app env accepted values ['dev', 'prod'] |
| BROKER_ADDRESS  | kafka  | kafka broker address |
| INPUT_TOPIC |  dev.pingpong.requested | input topic name|
| SUCCESS_TOPIC |  dev.pingpong.succeeded | success topic name |
| FAIL_TOPIC |  dev.pingpong.failed | fail topic name |
| EXPECTED_MESSAGE  |  ping | expected input message value  |

##### Makefile

Usage:
```sh
make <target>
```

| target              | description                                  |
| ------------------- | -------------------------------------------- |
| help                | show make file documentation                 |
| build               | build image to in-cluster container runtime  |
| deploy              | build -> deploy-kafka -> deploy-microservice |
| delete              | uninstall all charts                         |
| expose              | expose api at http://127.0.0.1:8080/docs"    |

### Development (docker-compose)

- Start developer stack with webserver hot-reload and DEBUG level logs

```sh
# start developer stack with webserver hot-reload and DEBUG level logs
docker-compose -f docker-compose.yml -f kafka/docker-compose.yml up -d --build

```

- Click here to access [API Documentation](http://127.0.0.1:8080/docs)

- Helpfull Commands

```sh
# open a bash terminal in kafka container
docker exec -it kafka /bin/bash

# crate default topics
kafka-topics.sh --create --topic dev.pingpong.requested --bootstrap-server localhost:9092
kafka-topics.sh --create --topic dev.pingpong.succeded --bootstrap-server localhost:9092
kafka-topics.sh --create --topic dev.pingpong.failed --bootstrap-server localhost:9092

# describe topic
kafka-topics.sh --describe --topic dev.pingpong.requested --bootstrap-server localhost:9092

# consume events from topic
kafka-console-consumer.sh --topic dev.pingpong.requested --from-beginning --bootstrap-server localhost:9092
```

## Architecture

### CI-CD
![pipeline](images/pipeline.png)

#### Notes
- Use rules in build step to publish new charts only if changes in helm folder are detected.
- Build docker image using previous images as cache.
- Run tests in parallel when possible.



### Infrastructure
![architecture](images/infrastructure.png)

#### Monitoring

##### Logs (ELK, loki+grafana)
  - Logs have a standardized format, shared with the rest of microservices.
  
##### Metrics (prometheus, cAdvisor)
  - Pull and store metrics from kubernetes api.
  - The metrics can be visualized in realtime unsing dashboard service like Grafana.
  - Main microservice metrics:
    - cpu usage
    - available memory
    - disk space

##### Exceptions tracking
  - Use a exception tracking like Sentry to catch code exception before final user 

##### Alerts (prometheus alert-manager, robusta, grafana)
  - send alerts: 
    - thresholds defined in rules for CPU usage, available memory are riched.
    - pods CrashLoopBackoffs.
    - filesystem disk space rich the limit.
  - Send alerts from sentry triggered by code exceptions.
#### Scale

##### Cluster autoscaling 
- Calculate resources requirements (cpu, memory) based on monitorig statistics.
- Set resources request and limits in deployment for each pod in deployment.
- Set number of nodes limits in cluster autoscaler based on budget/expected avg usage.  
- Choose instance types based on the microservice policies
  - use cheap spot instances for interuptable services
  - use on deman instances for services with high needed Qos
  - use mixed instance type policy


##### HPA
- set autoscaling parameters in values.yaml based on resource provisioning estimation. 

```
autoscaling:
  enabled: true
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
```
